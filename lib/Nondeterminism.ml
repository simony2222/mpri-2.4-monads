(* Once you are done writing the code, remove this directive,
   whose purpose is to disable several warnings. *)
[@@@warning "-27-32-33-37-39"]

module Base = struct
  type 'a t = 'a list

  let return a = [a]

  let rec bind m f = match m with
  | [] -> []
  | t::q -> (f t)@(bind q f) (* Selon ce que c'est censé faire, ça n'est pas si moche que ça. Au moins je couvre mes deux hypothèse avec cette formulation *)

end

module M = Monad.Expand (Base)
include M

let fail () = []

let either a b = a@b

let run m = match m with
| [] -> failwith "failed"
| t::q -> t

let rec all m = match m with (* je suis toujours perplex quand à l'utilité de cette fonction... (j'ai probablement pas vraiment compris sont but... *)
| [] -> Seq.empty
| t::q -> fun () -> Cons(t,all(q))

