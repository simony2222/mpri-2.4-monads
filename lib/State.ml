(* Once you are done writing the code, remove this directive,
   whose purpose is to disable several warnings. *)
[@@@warning "-27-32-33-37-39"]

module Make (S: sig 
                 type t
                 val init : t
               end) = struct

  module State = struct
    type 'a t = S.t -> 'a * S.t

    let return a = fun s -> (a, s)

    let bind m f s = let (a, s2) = m s in f a s2

  end

  module M = Monad.Expand (State)
  include M

  let get () s = (s, s)

  let set x _ = ((), x)
            
  let run m = let (a,_) = m S.init in a

end
