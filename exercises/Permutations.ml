(* Once you are done writing the code, remove this directive,
   whose purpose is to disable several warnings. *)
[@@@warning "-20-27-32-33-37-39"]

open Monads.Nondeterminism

let rec insert x l = match l with
| [] -> return [x]
| t::q -> let o = bind (insert x q) (fun q2 -> return (t::q2)) 
            in either (return (x::l)) o

let rec permut l = match l with
| [] -> return []
| [x] -> return [x]
| t::q -> bind (permut q) (insert t)


let%test _ = List.of_seq (all (permut [])) = [[]]
let%test _ = List.of_seq (all (permut [1])) = [[1]]
let%test _ = List.sort compare (List.of_seq (all (permut [1; 2]))) = [[1; 2]; [2; 1]]
let%test _ = List.sort compare (List.of_seq (all (permut [1; 2; 3]))) = [[1;2;3]; [1;3;2]; [2;1;3]; [2;3;1]; [3;1;2]; [3;2;1]]
