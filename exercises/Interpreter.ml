(* Once you are done writing the code, remove this directive,
   whose purpose is to disable several warnings. *)
[@@@warning "-20-27-32-37-39"]

open Monads.Error
exception WrongType

type value =
  | IsNat of int
  | IsBool of bool

type exp =
  | Val of value
  | Eq of exp * exp
  | Plus of exp * exp
  | Ifte of exp * exp * exp

(*let ( let* ) = Monads.Error.bind

let return = failwith "NYI: bring me in scope!"

let run = failwith "NYI: bring me in scope!"*)

let rec sem e = match e with
| Val(v) -> return v
| Eq(e1, e2) -> let* v1 = sem e1 in let* v2 = sem e2 in return (IsBool(v1=v2))
| Plus(e1,e2) -> let* v1 = sem e1 in let* v2 = sem e2 in
  begin match v1, v2 with
  | IsNat n1, IsNat n2 -> return (IsNat (n1+n2))
  | _, _ -> err WrongType
  end
| Ifte (e, e1, e2) -> let* v = sem e in
  match v with
  | IsBool true -> sem e1
  | IsBool false -> sem e2
  | _ -> err WrongType



(** * Tests *)

let%test _ = run (sem (Val (IsNat 42))) = IsNat 42

let%test _ = run (sem (Eq (Val (IsBool true), Val (IsBool true)))) = IsBool true

let%test _ = run (sem (Eq (Val (IsNat 3), Val (IsNat 3)))) = IsBool true

let%test _ =
  run (sem (Eq (Val (IsBool true), Val (IsBool false)))) = IsBool false

let%test _ = run (sem (Eq (Val (IsNat 42), Val (IsNat 3)))) = IsBool false

let%test _ = run (sem (Plus (Val (IsNat 42), Val (IsNat 3)))) = IsNat 45

let%test _ =
  run (sem (Ifte (Val (IsBool true), Val (IsNat 42), Val (IsNat 3)))) = IsNat 42

let%test _ =
  run (sem (Ifte (Val (IsBool false), Val (IsNat 42), Val (IsNat 3)))) = IsNat 3

let%test _ =
  run
    (sem
       (Ifte
          ( Eq (Val (IsNat 21), Plus (Val (IsNat 20), Val (IsNat 1)))
          , Val (IsNat 42)
          , Val (IsNat 3) )))
  = IsNat 42

(** ** Ill-typed expressions *)

let%test _ =
  try
    ignore(run (sem (Plus (Val (IsBool true), Val (IsNat 3)))));
    false
  with
    _ -> true

let%test _ =
  try
    ignore (run (sem (Ifte (Val (IsNat 3), Val (IsNat 42), Val (IsNat 44)))));
    false
  with
    _ -> true
