(* Once you are done writing the code, remove this directive,
   whose purpose is to disable several warnings. *)
[@@@warning "-20-27-32-33-37-39"]

open Monads

(* Taken from [https://wiki.haskell.org/State_Monad#Complete_and_Concrete_Example_1] *)

(* Passes a string of dictionary {a,b,c}
 * Game is to produce a number from the string.
 * By default the game is off, a 'c' toggles the
 * game on and off.
 * A 'a' gives +1 and a 'b' gives -1 when the game is on,
 * nothing otherwise.
 *)

 (* shamelessly stolen to continue *)

 

(*let ( let* ) = failwith "NYI: bring me in scope!"
let run = failwith "NYI: bring me in scope!"*)
(*open Monads.State*)

module M = State.Make(struct
                        type t = bool* int
                        let init = (false, 0)
                      end)
open M

let playGame s =
  let rec help i = 
    if i >= String.length s then
      let* (_, score) = get () in
      return score
    else
      let* (status, score) = get () in
      let x = String.get s i in
      let* _ = match x with
      | 'a' when status -> set (status, score + 1)
      | 'b' when status -> set (status, score - 1)
      | 'c' -> set(not status, score)
      | _ -> set (status, score)
      in
        help (i+1)
  in help 0


let result s = run (playGame s)

let%test _ = result "ab" = 0
let%test _ = result "ca" = 1
let%test _ = result "cabca" = 0
let%test _ = result "abcaaacbbcabbab" = 2
